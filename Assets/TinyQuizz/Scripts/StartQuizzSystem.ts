
namespace game {

    /** New System */
    export class StartQuizzSystem extends ut.ComponentSystem {

        OnUpdate():void {
            this.world.forEach([ut.Entity, StartNewQuizzTag, ut.UIControls.MouseInteraction], (entity, tag, mouseInteraction) => {
                if (mouseInteraction.clicked) {
                    // TODO: remove this entity (and children), call QuizzSystem
                    let destroyAfterDelay = new game.DestroyAfterDelay();
                    destroyAfterDelay.Delay = 0;
                    this.world.addComponentData(entity, destroyAfterDelay);
                    QuizzSystem.command = QuizzCommand.CreateNewQuizz;
                    QuizzTimerSystem.mustInstantiateTimer = true;
                }
            });
        }
    }
}
