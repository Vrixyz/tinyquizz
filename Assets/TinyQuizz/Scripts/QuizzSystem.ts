
namespace game {

    class QuizzEntry {
        question: string;
        answers: [string, boolean][];
        correctAnswer: number;
        constructor(question: string, answers: [string, boolean][]) { 
            this.question = question;
            this.answers = answers;
        }
    }
    export enum QuizzCommand {
        Idle = 1,
        CreateNewQuizz,
        EndQuizzGame,
    }

    export class QuizzEntitiesFilter extends ut.EntityFilter {
        quizzEntities: QuizzEntities;
        destroyAfterDelay?: DestroyAfterDelay;
    }

    /** New System */
    export class QuizzSystem extends ut.ComponentSystem {
        filter : QuizzEntitiesFilter = new QuizzEntitiesFilter();

        //static question: ut.Entity = null;
        //static answersTexts: Array<ut.Entity> = null;
        //static correctButtonAnswer: ut.Entity = null;
        static command = QuizzCommand.Idle;
        static quizzRespawnTimeoutId = 0;
		static currentScore = 0;

        static answersPool: Array<QuizzEntry> = new Array<QuizzEntry>(
            new QuizzEntry("Combien font 1+1 ?", [
                ["1", false],
                ["2", true],
                ["3", false],
                ["4", false]]),
            new QuizzEntry("Combien font 2+2 ?", [
                ["2", false],
                ["4", true],
                ["6", false],
                ["8", false]]),
            new QuizzEntry("Combien font 3+3 ?", [
                ["3", false],
                ["6", true],
                ["8", false],
                ["9", false]])
        );

        static SetNewQuizz(world: ut.World): void {
            let quizzEntity = ut.EntityGroup.instantiate(world, "game.QuizzGroup")[0];
            world.forEach([QuizzEntities], (quizzEntities) => {

                // Initialization
                let randomIndex = Math.floor(Math.random() * QuizzSystem.answersPool.length);
                let randomQuizzEntry = QuizzSystem.answersPool[randomIndex];
                world.usingComponentData(quizzEntities.question_text, [ut.Text.Text2DRenderer], (textRenderer) => {
                    textRenderer.text = randomQuizzEntry.question;
                });
                // FIXME: not a perfect shuffle, see https://javascript.info/task/shuffle
                let answers = randomQuizzEntry.answers.sort(() => Math.random() - 0.5);

                if (quizzEntities.answer_texts.length != quizzEntities.answer_buttons.length) {
                    console.log("Should not happen: answer_texts count is not equal to answer_buttons");
                }

                // Set QuizzEntities Data
                let currentAnswerDataIndex = 0;
                for (let i = 0; i < quizzEntities.answer_texts.length; i++) {
                    world.usingComponentData(quizzEntities.answer_texts[i], [ut.Entity, ut.Text.Text2DRenderer], (entity, textRenderer) => {
                        let current_answer = answers[currentAnswerDataIndex];
                        textRenderer.text = current_answer[0];
                        currentAnswerDataIndex++;
                        currentAnswerDataIndex %= answers.length;

                        if (current_answer[1]) {
                            let current_button = quizzEntities.answer_buttons[i];
                            quizzEntities.correct_answer_button = new ut.Entity(current_button.index, current_button.version);
                        }
                        world.usingComponentData(quizzEntities.answer_buttons[i], [AnswerHint], (answerHint) => {
                            answerHint.isCorrect = current_answer[1];
                            answerHint.isShown = false;
                        });
                    });
                }
            });
        }
        static EndQuizzGameQuizz(): void {
            clearTimeout(QuizzSystem.quizzRespawnTimeoutId);
            QuizzSystem.command = QuizzCommand.EndQuizzGame;
        }

        OnUpdate():void {
            this.filter.ForEach(this.world, (quizzEntity) => {
                let data = this.filter;
                if (data.destroyAfterDelay != null) {
                    return;
                }
                data.quizzEntities.answer_buttons.forEach((button) => {
                    this.world.usingComponentData(button, [ut.Entity, ut.UIControls.MouseInteraction], (buttonEntity, mouseInteraction) => {
                        if (mouseInteraction.clicked) {
                            data.quizzEntities.isDone = true;
                            if (data.quizzEntities.correct_answer_button && buttonEntity.index == data.quizzEntities.correct_answer_button.index
                                && buttonEntity.version == data.quizzEntities.correct_answer_button.version) {
                                data.quizzEntities.isWin = true;
								QuizzSystem.currentScore++;
								UpdateScoreSystem.UpdateScore(this.world, QuizzSystem.currentScore);
                            }
                            else {
                                data.quizzEntities.isWin = false;
                                //ut.EntityGroup.instantiate(this.world, "game.QuizzLostGroup");
                            }
                        }
                    });
                });
                if (data.quizzEntities.isDone) {
                    data.quizzEntities.answer_buttons.forEach((button) => {
                        this.world.usingComponentData(button, [AnswerHint], (answerHint) => {
                            answerHint.isShown = true;
                        });
                    });
                    let destroyAfterDelay = new game.DestroyAfterDelay();
                    destroyAfterDelay.Delay = 1;
                    this.world.addComponentData(quizzEntity, destroyAfterDelay);
                    QuizzSystem.quizzRespawnTimeoutId = setTimeout(function () {
                         QuizzSystem.command = QuizzCommand.CreateNewQuizz;
                    }, 1000);
                }
            });
            if (QuizzSystem.command == QuizzCommand.CreateNewQuizz) {
                QuizzSystem.SetNewQuizz(this.world);
                QuizzSystem.command = QuizzCommand.Idle;
				UpdateScoreSystem.UpdateScore(this.world, QuizzSystem.currentScore);
            }
            else if (QuizzSystem.command == QuizzCommand.EndQuizzGame) {
                console.log("Should create new game");
				QuizzSystem.currentScore = 0;
				UpdateScoreSystem.UpdateScore(this.world, QuizzSystem.currentScore);
                QuizzSystem.command = QuizzCommand.Idle;
                ut.EntityGroup.destroyAll(this.world, "game.QuizzGroup");
                ut.EntityGroup.destroyAll(this.world, "game.QuizzGameTimerGroup");
                ut.EntityGroup.instantiate(this.world, "game.QuizzMenuGroup");
            }
        }
    }
}
