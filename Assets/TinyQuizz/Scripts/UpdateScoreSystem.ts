
namespace game {

    /** New System */
    export class UpdateScoreSystem {
        
        public static UpdateScore(world: ut.World, score: number):void {
			world.forEach([ScoreTag, ut.Text.Text2DRenderer], (tag, textRenderer) => {
				textRenderer.text = "Score: " + score;
			});
        }
    }
}
