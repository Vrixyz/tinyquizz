
namespace game {

    /** New System */
    export class QuizzTimerSystem extends ut.ComponentSystem {

        static mustInstantiateTimer = true;

        OnUpdate():void {
            if (QuizzTimerSystem.mustInstantiateTimer) {
                ut.EntityGroup.instantiate(this.world, "game.QuizzGameTimerGroup");
                QuizzTimerSystem.mustInstantiateTimer = false;
                this.world.forEach([game.QuizzTimer], (quizzTimer) => {
                    quizzTimer.currentTimeLeft = quizzTimer.totalTime;
                });
            }

            this.world.forEach([game.QuizzTimer, ut.Core2D.TransformLocalScale], (quizzTimer, scale) => {
                quizzTimer.currentTimeLeft -= this.scheduler.deltaTime();
                if (quizzTimer.currentTimeLeft <= 0) {
                    QuizzSystem.EndQuizzGameQuizz();
                    quizzTimer.currentTimeLeft = quizzTimer.totalTime;
                }
                let new_scale = scale.scale;
                new_scale.x = quizzTimer.currentTimeLeft / quizzTimer.totalTime;
                scale.scale = new_scale;
            });
        }
    }
}
