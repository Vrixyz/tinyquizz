
namespace game {

    /** New System */
    export class HintSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.Entity, AnswerHint, ut.Core2D.Sprite2DRenderer, ut.UIControls.Button],
                (entity, answerHint, spriteRenderer, button) => {
                if (answerHint.isShown) {
                    this.world.removeComponent(entity, ut.UIControls.Button);
                    console.log("showing hint");
                    let color = spriteRenderer.color;
                    if (answerHint.isCorrect) {
                        color.r = 0;
                        color.g = 1;
                        color.b = 0;
                    spriteRenderer.color = new ut.Core2D.Color(0,1,0,1);
                    }
                    else {
                    spriteRenderer.color = new ut.Core2D.Color(1,0,0,1);
                        color.r = 1;
                        color.g = 0;
                        color.b = 0;
                    }
                }
            });
        }
    }
}
